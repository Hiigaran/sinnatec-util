package com.sinnatec.crypto.sm2;

import java.io.Serializable;

public class SM2KeyPair implements Serializable {
    private static final long serialVersionUID = 1L;

    private String privateKey;

    private String publicKey;

    public SM2KeyPair(){

    }

    public SM2KeyPair(String privateKey, String publicKey) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public String toString() {
        return "SM2KeyPair{" +
                "privateKey='" + privateKey + '\'' +
                ", publicKey='" + publicKey + '\'' +
                '}';
    }
}
