package com.sinnatec;


import com.sinnatec.crypto.sm4.SM4Util;
import com.sinnatec.math.DecimalUtil;

import java.math.BigDecimal;


public class TestMain {
    public static void main(String [] arg) throws Exception {
        System.out.println(DecimalUtil.getDecimal(16.1));
        System.out.println(new BigDecimal(16.1));
    }
}
