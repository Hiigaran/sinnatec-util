package com.sinnatec.lang;

import org.apache.commons.lang3.StringUtils;

public class StringUtil extends StringUtils {

    public static final String EQUAL_SIGN = "=";
    public static final String AMPERSAND = "&";
    public static final String SEMICOLON = ";";
    public static final String COMMA = ",";
    private StringUtil(){

    }

    public static String nullToEmpty(String str){
        if(str==null){
            return EMPTY;
        }
        return str;
    }
}
