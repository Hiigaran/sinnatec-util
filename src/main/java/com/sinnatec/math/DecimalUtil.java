package com.sinnatec.math;

import com.sinnatec.lang.ObjectUtil;

import java.math.BigDecimal;

public class DecimalUtil {

    private DecimalUtil(){

    }

    public static BigDecimal getDecimal(Object o){
        return new BigDecimal(ObjectUtil.toString(o));
    }
}
