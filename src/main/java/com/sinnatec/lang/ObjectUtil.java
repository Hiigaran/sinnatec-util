package com.sinnatec.lang;


import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ObjectUtil {

    private ObjectUtil(){

    }

    /**
     * 判断Object是否基本数据类型或者其包装类
     * @param object
     *                  对象
     * @return
     *                  判断结果
     */
    public static boolean isBaseType(Object object) {
        Class<?> className = object.getClass();
        return object.getClass().isPrimitive()||
                className.equals(Integer.class) ||
                className.equals(Byte.class) ||
                className.equals(Long.class) ||
                className.equals(Double.class) ||
                className.equals(Float.class) ||
                className.equals(Character.class) ||
                className.equals(Short.class) ||
                className.equals(Boolean.class);
    }

    /**
     * Object转Long
     *
     * @param obj
     *                      要转化的值
     * @return
     *                      转化过的值
     */
    public static Long toLong(Object obj) {
        return toLong(obj, null);
    }

    /**
     * Object转Long
     *
     * @param obj
     *                      要转化的值
     * @param defaultValue
     *                      默认值
     * @return
     *                      转化过的值
     */
    public static Long toLong(Object obj, Long defaultValue) {
        if (obj == null) {
            return defaultValue;
        } else {
            try {
                if (obj instanceof String) {
                    return Long.valueOf((String) obj);
                } else if (obj instanceof Short) {
                    return ((Short) obj).longValue();
                } else if (obj instanceof Integer) {
                    return ((Integer) obj).longValue();
                } else if (obj instanceof Long) {
                    return ((Long) obj);
                } else if (obj instanceof Float) {
                    return ((Float) obj).longValue();
                } else if (obj instanceof Double) {
                    return ((Double) obj).longValue();
                } else if (obj instanceof BigDecimal) {
                    return ((BigDecimal) obj).longValue();
                } else {
                    return defaultValue;
                }
            } catch (Exception e) {
                return defaultValue;
            }
        }
    }

    /**
     * Object转Integer
     *
     * @param obj
     *                      要转化的值
     * @return
     *                      转化过的值
     */
    public static Integer toInteger(Object obj) {
        return toInteger(obj, null);
    }

    /**
     * Object转Integer
     *
     * @param obj
     *                      要转化的值
     * @param defaultValue
     *                      默认值
     * @return
     *                      转化过的值
     */
    public static Integer toInteger(Object obj, Integer defaultValue){
        if (obj == null) {
            return defaultValue;
        } else {
            try {
                if (obj instanceof String) {
                    return Integer.valueOf((String) obj);
                } else if (obj instanceof Short) {
                    return  ((Short) obj).intValue();
                } else if (obj instanceof Integer) {
                    return (Integer) obj;
                } else if (obj instanceof Long) {
                    return ((Long) obj).intValue();
                } else if (obj instanceof Float) {
                    return ((Float) obj).intValue();
                } else if (obj instanceof Double) {
                    return ((Double) obj).intValue();
                } else if (obj instanceof BigDecimal) {
                    return ((BigDecimal) obj).intValue();
                } else {
                    return defaultValue;
                }
            } catch (Exception e) {
                return defaultValue;
            }
        }
    }

    /**
     * Object转Float
     *
     * @param obj
     *                      要转化的值
     * @return
     *                      转化过的值
     */
    public static Float toFloat(Object obj) {
        return toFloat(obj, null);
    }

    /**
     * Object转Float
     *
     * @param obj
     *                      要转化的值
     * @param defaultValue
     *                      默认值
     * @return
     *                      转化过的值
     */
    public static Float toFloat(Object obj, Float defaultValue) {
        if (obj == null) {
            return defaultValue;
        } else {
            try {
                if (obj instanceof String) {
                    return Float.valueOf((String) obj);
                } else if (obj instanceof Float) {
                    return (Float) obj;
                } else if (obj instanceof Long) {
                    return ((Long) obj).floatValue();
                } else if (obj instanceof Integer) {
                    return ((Integer) obj).floatValue();
                } else if (obj instanceof Short) {
                    return ((Short) obj).floatValue();
                } else if (obj instanceof Double) {
                    return ((Double) obj).floatValue();
                } else if (obj instanceof BigDecimal) {
                    return ((BigDecimal) obj).floatValue();
                } else {
                    return defaultValue;
                }
            } catch (Exception e) {
                return defaultValue;
            }
        }
    }

    /**
     * Object转Double
     *
     * @param obj
     *                      要转化的值
     * @return
     *                      转化过的值
     */
    public static Double toDouble(Object obj) {
        return toDouble(obj, null);
    }

    /**
     * Object转Double
     *
     * @param obj
     *                      要转化的值
     * @param defaultValue
     *                      默认值
     * @return
     *                      转化过的值
     */
    public static Double toDouble(Object obj, Double defaultValue) {
        if (obj == null) {
            return defaultValue;
        } else {
            try {
                if (obj instanceof String) {
                    return Double.valueOf((String) obj);
                } else if (obj instanceof Float) {
                    return ((Float) obj).doubleValue();
                } else if (obj instanceof Long) {
                    return ((Long) obj).doubleValue();
                } else if (obj instanceof Integer) {
                    return ((Integer) obj).doubleValue();
                } else if (obj instanceof Short) {
                    return ((Short) obj).doubleValue();
                } else if (obj instanceof Double) {
                    return ((Double) obj);
                } else if (obj instanceof BigDecimal) {
                    return ((BigDecimal) obj).doubleValue();
                } else {
                    return defaultValue;
                }
            } catch (Exception e) {
                return defaultValue;
            }
        }
    }

    /**
     * Object转Boolean
     *
     * @param obj
     *                      要转化的值
     * @return
     *                      转化过的值
     */
    public static Boolean toBoolean(Object obj){
        return toBoolean(obj,null);
    }

    /**
     * Object转Boolean
     *
     * @param obj
     *                      要转化的值
     * @param defaultValue
     *                      默认值
     * @return
     *                      转化过的值
     */
    public static Boolean toBoolean(Object obj,Boolean defaultValue){
        if (obj == null) {
            return defaultValue;
        } else {
            try {
                if (obj instanceof String) {
                    return Boolean.valueOf((String)obj);
                } else if (obj instanceof Boolean) {
                    return (Boolean) obj;
                } else {
                    return defaultValue;
                }
            } catch (Exception e) {
                return defaultValue;
            }
        }
    }

    public static String toString(Object obj) {
        return toString(obj, null);
    }

    public static String toString(Object obj, String defaultValue) {
        if (obj == null) {
            return defaultValue;
        } else {
            try {
                if (obj instanceof String) {
                    return (String) obj;
                } else if (obj instanceof byte[]) {
                    return new String((byte[]) obj);
                } else {
                    return String.valueOf(obj);
                }
            } catch (Exception e) {
                return defaultValue;
            }
        }
    }

    public static boolean equals(Object o1,Object o2){
        if(o1==null&&o2==null){
            return true;
        }
        if(o1==null||o2==null){
            return false;
        }
        return o1.equals(o2);
    }

}
